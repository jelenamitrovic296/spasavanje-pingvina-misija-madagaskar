#include "./mainwindow.h"
#include "ui_mainwindow.h"
#include "./secondWindow.h"

#include <QGraphicsScene>
#include <QGraphicsView>
#include "../game/player1.h"

#include "../game/level.h"
#include "../game/game.h"

#include<QDebug>

Game *game;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui ->stackedWidget ->setCurrentIndex(0);
    this ->setWindowTitle("PINGVINI SA MADAGASKARA");
    this->setFixedSize(800, 600);

    //Pozadina
    bSetter = new backgroundSetter(this, ":/resources/background.jpg");
    bSetter->setNewBackground();

   //u konstruktoru inicijalizujemo klasu settings
    mSetter = new MusicSetter();
    rangList = new RangList();


    //postavljanje stoperice
    //ui->lcdNTimer->setFixedSize(100,50);
    //ui->lcdNTimer->move(10,10);
    //konekcija

    connect(ui->pbStartOne, SIGNAL(clicked()), this, SLOT(startStopwatch()));
    connect(&stopwatch_, SIGNAL(timeChanged(const QTime&)), this, SLOT(updateStopwatchTime(const QTime&)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pbClose_clicked()
{
    close();
}


void MainWindow::on_pbStart_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(3);

    this ->setWindowTitle("PODACI O IGRI");
    this->setFixedSize(800, 600);

    bSetter = new backgroundSetter(ui->page3Names, ":/resources/nameSurname.jpeg");
    bSetter->setNewBackground();

    connect(ui->pbStart, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);

}

void MainWindow::on_pbAbout_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(1);
    //Pozadina

    this ->setWindowTitle("ABOUT GAME");
    this->setFixedSize(800, 600);

    bSetter= new backgroundSetter(ui->page2AboutGame, ":/resources/aboutGame.jpg");
    bSetter->setNewBackground();

    connect(ui->pbAbout, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);

}

void MainWindow::on_pbOK_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(0);
}

void MainWindow::on_pbBack_clicked()
{
    ui ->stackedWidget ->setCurrentIndex(0);

}

void MainWindow::on_pbStart2_clicked()
{

   // ui ->stackedWidget ->setCurrentIndex(2);
   // Level* level = new Level();
    //level->startLevel();
    if(ui->rbOnePlayer->isChecked()){
        ui->stackedWidget->setCurrentIndex(2);
        bSetter = new backgroundSetter(ui->page4Game, ":/resources/nameSurname.jpeg");
        bSetter->setNewBackground();

        connect(ui->pbStart, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);
    }else if(ui->rbTwoPlayers->isChecked()){
        ui->stackedWidget->setCurrentIndex(2);

        bSetter = new backgroundSetter(ui->page4Game, ":/resources/nameSurname.jpeg");
        bSetter->setNewBackground();
        connect(ui->pbStart, &QPushButton::clicked, bSetter, &backgroundSetter::setNewBackground);

        SecondWindow *secondWindow = new SecondWindow();
        secondWindow->show();
    }
}

void MainWindow::on_pbSettings_clicked()
{
      ui ->stackedWidget ->setCurrentIndex(4);
}

void MainWindow::on_rbOnMusic_toggled(bool checked)
{
      if(checked){
        mSetter->onMusic();

      }
}

void MainWindow::on_rbOffMusic_toggled(bool checked)
{
      if(checked){
        mSetter->offMusic();
      }
}

void MainWindow::on_hsVolume_sliderMoved(int position)
{
      mSetter->setVolume(position);

}

void MainWindow::on_pbOkMusic_clicked()
{
      ui->stackedWidget->setCurrentIndex(0);

}

void MainWindow::on_pbRangList_clicked()
{
      QStringList currentRow;

      ui->stackedWidget->setCurrentIndex(5);
      //u posebnoj fji samo dohvatamo podatke.U njoj ne mozemo da popunimo tabelu, zato sto nemamo pristup ui main window
      QStringList rows = rangList->getData();
      for(int i=0;i<10 && i< rows.size()-1;i++)
      {
        currentRow=rows.at(i).split(" ");
        for(int j=0;j<2;j++)
        {
            ui->tbRangList->setItem(i,j,new QTableWidgetItem(currentRow[j].trimmed()));
        }
      }
}

void MainWindow::on_rbOnePlayer_clicked()
{
      ui->rbTwoPlayers->setChecked(false);
      ui->rbOnePlayer->setChecked(true);
}


void MainWindow::on_rbTwoPlayers_clicked()
{
      ui->rbOnePlayer->setChecked(false);
      ui->rbTwoPlayers->setChecked(true);
}


void MainWindow::on_pbStartOne_clicked()
{
      QString name1 = ui->teOnePlayer->toPlainText();
      if(name1!=""){
        game = new Game();
        game->show();
        ui->lMessage->setText("");
        hide();
      }else{
          ui->lMessage->setText("You did not enter a name!");
      }
}


void MainWindow::on_pbBackOne_clicked()
{
      ui ->stackedWidget ->setCurrentIndex(3);
      ui->lMessage->setText("");
}


void MainWindow::on_pbBackRL_clicked()
{
      ui->stackedWidget->setCurrentIndex(0);


}
void MainWindow::startStopwatch()
{
      stopwatch_.start();
}

void MainWindow::updateStopwatchTime(const QTime& time)
{
      //ui->lcdNTimer->display(time.toString("hh:mm:ss"));
}
