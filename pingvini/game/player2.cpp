#include "player2.h"
#include <QGraphicsScene>
#include <QKeyEvent>

Player2::Player2() : QGraphicsPixmapItem()
{
    setPixmap(QPixmap(":/resources/pingvin2.png"));
}

void Player2::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_A){
        if (pos().x() > 0)
            setPos(x()-10, y());
    }
    else if(event->key() == Qt::Key_D){
        if (pos().x() + 100 < 850)
            setPos(x()+10, y());
    }
}
