#include "rangList.h"
#include <QFile>
#include <QDebug>
RangList::RangList()
{
    rangListFilePath = "../pingvini/res.txt";

}

QStringList RangList::getData()
{
    QFile file(rangListFilePath);

    QString data;
    QStringList rows;
    data.clear();
    rows.clear();


    if(file.open(QFile::ReadOnly))
    {
        data=file.readAll();
        rows =data.split("\n");
        file.close();
    }
    return rows;
}

void RangList::addToRangList(QString currentName, int currentScore)
{
    int pos = 0;
    QFile file(rangListFilePath);
    QString data;
    QStringList rows,row;
    data.clear();
    rows.clear();
    row.clear();

    if(!file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        qDebug() << file.errorString();
    }else{
        data=file.readAll();
        rows=data.split("\n");
    }
    //trazimo poziciju,fajl je sortiran po poenima opadajuce
    for(int i=0;i<rows.size()-1;i++){
        row =rows.at(i).split(" ");
        int score =row.at(1).toInt();
        if(currentScore<score){
            pos=pos+1;
        }
    }

    QTextStream stream(&file);
    stream.seek(0);
    for(int i=0;i<pos;i++){
        stream<<rows.at(i)<<"\n";
    }
    stream<<currentName <<" "<<currentScore <<"\n";
    for(int i=pos;i<rows.size()-1;i++){
        stream<< rows.at(i)<<"\n";
    }
    file.close();
}
