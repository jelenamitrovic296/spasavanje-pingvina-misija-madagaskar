#include "./picturesMapLevel.h"


picturesMapLevel::picturesMapLevel()
{
    setUpMap();
}

void picturesMapLevel::setUpMap(){
    map['P'] = ":/resources/platform_resized4.png";
    map['L'] = ":/resources/lemur.png";
    map['C'] = ":/resources/kavez_resize.png";
    map['G'] = ":/resources/guard.png";
    map['H'] = ":/resources/hole_resized.png";
    map['K'] = ":/resources/key_resized.png";
    map['$'] = ":/resources/player_resized.png";
    map['F'] = ":/resources/fish.png";


    yAxisDrop['G'] = 0.3;
    yAxisDrop['L'] = 0.2;
}

QString picturesMapLevel::getPath(char symbol){
    return map[symbol];
}

void picturesMapLevel::setUpCharacter(char symbol){
    setPixmap(QPixmap(map[symbol]));
}

qreal picturesMapLevel::getScalingFactor(char symbol){
    return scalingFactor[symbol];
}

qreal picturesMapLevel::getYDrop(char symbol){
    return yAxisDrop[symbol];
}
