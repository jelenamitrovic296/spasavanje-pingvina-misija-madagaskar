#include "backgroundSetter.h"

backgroundSetter::backgroundSetter(QWidget *widget, const QString &imagePath)
    :QObject(widget), targetWidget(widget)
{
    backgroundPixmap.load(imagePath);
    //Prilagodi velicinu slike velicini prozora
    backgroundPixmap = backgroundPixmap.scaled(targetWidget->size(), Qt::IgnoreAspectRatio);
}

void backgroundSetter::setNewBackground()
{
    // Postavi sliku kao pozadinu
    QPalette palette;
    palette.setBrush(QPalette::Window, backgroundPixmap);

    targetWidget->setAutoFillBackground(true);
    targetWidget->setPalette(palette);

    // Postavi veličinu stranice da odgovara veličini prozora
    targetWidget->setFixedSize(targetWidget->size());
}
