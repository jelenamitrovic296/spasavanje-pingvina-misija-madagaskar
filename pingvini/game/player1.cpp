#include "./player1.h"
#include <QKeyEvent>

Player1::Player1() : QGraphicsPixmapItem()
{
    setPixmap(QPixmap(":/resources/pingvin1.png"));
}

void Player1::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Left){
        if(x() > 0){
            setPos(x()-10, y());

        }
    }
    else if(event->key() == Qt::Key_Right){
            setPos(x()+10, y());
    }

    emit playerMoved(pos().x());

}
