#include "game.h"
#include <QTimer>
#include <QGraphicsTextItem>
#include <QFont>

#include <QGraphicsView>
#include <QGraphicsScene>

Game::Game()
{
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,800,600);
    setBackgroundBrush(QBrush(QImage(":/resources/sky1.jpg")));

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(sceneSizeX,sceneSizeY);

    //postavljanje igraca 1
    player = new Player1();
    player->setPos(0,440);
    player->setFlag(QGraphicsItem::ItemIsFocusable);
    player->setFocus();
    scene->addItem(player);

    //postavljanje lemura

    lemur = new Lemur();


    scene->addItem(lemur);

    //Tajmer
    stopwatch_ = new stopwatch(this);
    timerLabel = new QLabel();
    timerLabel->setGeometry(10, 10, 100, 30);
    scene->addWidget(timerLabel);


    QObject::connect(stopwatch_, &stopwatch::timeChanged, this, [=]() mutable {
        lemur->move();
        //timerLabel->setText(stopwatch_->getTimeElapsed().toString("mm:ss"));
    });





    connect(stopwatch_, &stopwatch::timeChanged, this, &Game::updateTime);
    stopwatch_->start();
    connect(player,&Player1::playerMoved,this,&Game::moveBackground);








    scene->addWidget(timerLabel);


    view = new QGraphicsView(scene);

   // QString backgroundImage = ":/resources/sky1.jpg";
   // view->setBackgroundBrush(QPixmap(backgroundImage).scaledToHeight(600));

    view->setFocus();

    Level* level = new Level(scene, view);
    level->startLevel();


}
void Game::updateTime(QTime newTime)
{
    QString timeString = newTime.toString("mm:ss");
    timerLabel->setText("Time: " + timeString);
}
void Game::moveBackground(int playerX)
{
    //qDebug() << playerX;
    int viewX = view->mapToScene(view->viewport()->geometry().topLeft()).x();
    int deltaX = playerX - viewX;


    if ((deltaX < 100)  || (deltaX > 700)) {
        scene->setSceneRect(playerX, 100, view->viewport()->width(), view->viewport()->height());
    }

}
