#ifndef BACKGROUNDSETTER_H
#define BACKGROUNDSETTER_H

#include <QObject>
#include <QPixmap>
#include <QWidget>

class backgroundSetter: public QObject
{
    Q_OBJECT
public:
    backgroundSetter(QWidget *widget, const QString &imagePath);

public slots:
    void setNewBackground();

private:
    QWidget *targetWidget;
    QPixmap backgroundPixmap;
};

#endif // BACKGROUNDSETTER_H
