#include "stopwatch.h"

stopwatch::stopwatch(QObject *parent) : QObject(parent)
{
    timer = new QTimer(this);
    //connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    connect(timer, &QTimer::timeout, this, &stopwatch::update);
}

stopwatch::~stopwatch()
{
    delete timer;
}

void stopwatch::start()
{
    timeElapsed = QTime(0, 0);
    timer->start(1000);
}

void stopwatch::stop()
{
    timer->stop();
}
QTime stopwatch::getTimeElapsed() const
{
    return timeElapsed;
}


void stopwatch::update()
{
    timeElapsed = timeElapsed.addSecs(1);
    emit timeChanged(timeElapsed);
}

