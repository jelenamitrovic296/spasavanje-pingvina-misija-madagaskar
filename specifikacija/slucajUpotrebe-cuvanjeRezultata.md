# Slučaj upotrebe „Čuvanje rezultata“

## Kratak opis
Po uspešnom završetku igre, aplikacija koristi ime koje je uneto pre početka igre i skladišti informacije o odigranoj igri.

## Akteri
- Igrač – uspešno je završio igru

## Preduslovi
- Aplikacija je pokrenuta i igrač je prešao nivo uspešno.

## Postuslovi
- Informacije o odigranoj igri su trajno sačuvane.

## Osnovni tok
1. Igrač je kliknuo na dugme  "One player“
   1.1 Nakon završene igre proverava se da li je broj osvojenih bodova veći od najmanje osvojenih bodova u tabeli \
      1.1.1 Ako je uslov ispunjen, aplikacija čuva ime igrača i broj bodova \
      1.1.2 Ako uslov nije ispunjen, prelazi se na korak 2 \
2. Igrač je kliknuo na dugme "Two players“ \
   2.1 Nakon završene igre pobednik prelazi na korak 1.1.2
3. Aplikacija prikazuje glavni meni

## Alternativni tokovi
- Neočekivani izlaz iz aplikacije. Ako u bilo kom koraku korisnik isključi aplikaciju, sve eventualno zapamćene informacije o trenutnoj igri se poništavaju i aplikacija završava rad. Slučaj upotrebe se završava.

## Podtokovi
- /

## Specijalni uslovi
- /

## Dodatne informacije
- /
