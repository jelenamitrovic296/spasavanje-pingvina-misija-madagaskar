#ifndef RANGLIST_H
#define RANGLIST_H
#include<QString>

class RangList
{
public:
    RangList();
    QStringList getData();
    void addToRangList(QString name,int score);
private:
    QString rangListFilePath;

};

#endif // RANGLIST_H
