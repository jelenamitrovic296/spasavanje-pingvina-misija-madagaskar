#ifndef LEVEL_H
#define LEVEL_H

#include <QtGlobal>
#include <QFile>
#include<QDebug>
#include <QGraphicsView>
#include <QPixmap>
#include <QUrl>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QWidget>

 #include <QTextStream>

#include "picturesMapLevel.h"
#include "mapitem.h"

#include "game.h"


class Level : public QGraphicsView
{
public:
    Level();
    Level(QGraphicsScene* scene, QGraphicsView* view);

    void startLevel();
    void parseLevelMap();
    void addObject(char symbol, qreal x, qreal y);


    QGraphicsView* view;
    QGraphicsScene* scene;

private:
    int sceneSizeX;
    int sceneSizeY;

};

#endif // LEVEL_H
