#ifndef MUSICSETTER_H
#define MUSICSETTER_H
#include<QSoundEffect>

class MusicSetter
{
public:
    MusicSetter();
    void onMusic();
    void offMusic();
    void setVolume(int position);
private:
    QSoundEffect *music;
};

#endif // MUSICSETTER_H
