#ifndef LEMUR_H
#define LEMUR_H
#include <QGraphicsPixmapItem>
#include <QObject>
#include "stopwatch.h"

class Lemur : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Lemur(QGraphicsItem *parent = nullptr);

public slots:
    void move();

private:
    int direction;
    qreal distance;
    qreal currentDistance;
};

#endif // LEMUR_H

