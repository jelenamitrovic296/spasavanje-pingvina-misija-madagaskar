#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <QObject>
#include <QTimer>
#include <QTime>

class stopwatch : public QObject
{
    Q_OBJECT

public:
    stopwatch(QObject *parent = nullptr);
    ~stopwatch();

    void start();
    void stop();
    QTime getTimeElapsed() const;


signals:
    void timeChanged(const QTime& time);

private slots:
    void update();

private:
    QTimer *timer;
    QTime timeElapsed;
};

#endif // STOPWATCH_H
