QT       += core gui multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ./game/level.cpp \
    ./main.cpp \
    ./gui/mainwindow.cpp \
    ./game/mapitem.cpp \
    ./game/picturesMapLevel.cpp \
    ./game/player1.cpp \
    ./gui/secondWindow.cpp \
    game/game.cpp \
    game/lemur.cpp \
    game/player2.cpp \
    game/stopwatch.cpp \
    gui/backgroundSetter.cpp \
    game/rangList.cpp \
    gui/musicSetter.cpp

HEADERS += \
    ./gui/mainwindow.h \
    ./game/mapitem.h \
    ./game/picturesMapLevel.h \
    ./game/player1.h \
    ./gui/secondWindow.h \
    ./game/level.h \ \
    game/game.h \
    game/lemur.h \
    game/player2.h \
    game/stopwatch.h \
    gui/backgroundSetter.h \
    game/rangList.h \
    gui/musicSetter.h

FORMS += \
    ./gui/mainwindow.ui \
    ./gui/secondWindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
DISTFILES += \
    ../../../../Lightfox - Pluck (Official Music Video).wav \
    ../Desktop/pingvini/pingvinite-ot-madagaskar-blu-ray-31.png \
    Lightfox - Pluck (Official Music Video).wav \
    Lightfox - Pluck (Official Music Video).wav \
    res.txt \
    res_songs/Lightfox - Pluck (Official Music Video).wav \
    res_songs/Lightfox - Pluck (Official Music Video).wav \
    res_songs/Lightfox - Pluck (Official Music Video).wav
