#ifndef GAME_H
#define GAME_H

#include "./player1.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QLabel>

#include "./level.h"
#include "./game/stopwatch.h"
#include "./lemur.h"

class Game: public QGraphicsView
{
public:
    Game();

    QGraphicsScene *scene;
    Player1 *player;
    QGraphicsView* view;

    const static int sceneSizeX = 2000;
    const static int sceneSizeY = 500;

private:
    stopwatch *stopwatch_;
    QLabel *timerLabel;

    Lemur *lemur;


private slots:
    void updateTime(QTime newTime);
     void moveBackground(int playerX);
};


#endif // GAME_H
