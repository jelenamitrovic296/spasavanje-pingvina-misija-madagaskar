#include "./gui/mainwindow.h"

#include <QApplication>
#include <QLabel>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    w.setStyleSheet("background-image: url(qrc:/resources/background.jpg); background-position: center;"
                    "background-repeat: no-repeat");

    w.show();


    return a.exec();
}
