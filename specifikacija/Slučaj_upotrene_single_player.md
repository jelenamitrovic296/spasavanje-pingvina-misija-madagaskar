# Slučaj upotrebe "Igranje singleplayer partije"

## Kratak opis

Igrač iz glavnog menija klikom na dugme start ima opciju da bira igranje singleplayer partije i upiše svoje ime. Igra traje dok igrač ne stigne do cilja, nakon čega se vrši upis rezultata, pa se igraču prikazuje glavni meni, ili dok ga u igri ne savlada neka prepreka i igrač bude primoran da završi igru i vrati na glavni meni.

## Akteri

Jedan igrač

## Preduslovi

Aplikacija je pokrenuta i otvoren je glavni meni.

## Postuslovi

Aplikacija vraća igrača u glavni meni.

## Glavni tok

1. Igrač iz glavnog menija bira dugme za START.
2. Igraču se omogućava da izabere opciju SINGLE PLAYER.
3. Igraču se omogućava da upiše svoje ime.
4. Igrač pokreće partiju i prelazi na slučaj upotrebe "Igra".
5. Igrač završava igru i šalje se informacija o završenoj partiji
6. Ako ukupno vreme nije 0:
- 6.1. Igrač stigao do cilja, uspešno završio igru i prelazi se na slučaj upotrebe "Čuvanje rezultata".
-  6.2 Inače je igrača savladala prepreka i prelazi se na korak 7.
7. Aplikacija prikazuje glavni meni.

## Alternativni tok

- **A1:** Neočekivan izlaz iz aplikacije. Igra se završava.

## Podtokovi

Nema specifičnih podtokova.

## Specijalni zahtevi
Nema specijalnih zahteva

## Napomena: /
