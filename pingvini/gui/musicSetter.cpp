#include "./gui/musicSetter.h"
#include<QSoundEffect>

MusicSetter::MusicSetter()
{
    music = new QSoundEffect();
}

void MusicSetter::onMusic()
{

    music->setSource(QUrl::fromLocalFile(":/file_example_WAV_1MG.wav"));
    music->setLoopCount(QSoundEffect::Infinite);
    music->setVolume(50);
    music->play();

}
void MusicSetter::offMusic()
{
      music->stop();
}
void MusicSetter::setVolume(int position)
{
      music->setVolume(position*15);
}


