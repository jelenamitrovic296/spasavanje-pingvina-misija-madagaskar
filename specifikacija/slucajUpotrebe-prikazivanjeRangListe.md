# Slučaj upotrebe „Prikazivanje rang liste“

## Opis:
   Nakon što igrač pritisne dugme u glavnom meniju, prikazuje se tabela najboljih rezultata ostvarenih tokom igranja partija.

## Akteri:
   - Igrač – bira opciju iz glavnog menija

## Preduslovi:
   - Aplikacija je pokrenuta. Glavni meni je otvoren i prikazan.

## Postuslovi:
   - Prikazuju se informacije o igračima koji su odigrali najbolje rangirane partije i o broju poena koje su prikupili u tim partijama.

## Glavni tok:
1. Igrač pritiska dugme u glavnom meniju za prikaz rezultata.
2. Aplikacija prikazuje prozor na kojem su prikazani rezultati najbolje odigranih partija i informacije o igračima u obliku liste.
3. Igrač pritiska dugme koje ga vraća na prikaz glavnog menija.
4. Aplikacija prikazuje glavni meni.

## Alternativni tok:
   - Neočekivani izlaz iz aplikacije. Ako igrač isključi aplikaciju prilikom bilo kojeg koraka, aplikacija se gasi, a tabela najboljih rezultata ostaje nepromenjena.

## Podtokovi: /
## Specijalni uslovi: /
## Dodatne informacije: /
