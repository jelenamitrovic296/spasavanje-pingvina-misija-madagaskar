# Slučaj upotrebe "Igranje multiplayer partije"

**Kratak opis:**
Nakon što je odabrana opcija za igranje multiplayer partije, oba igrača unose svoja imena i klikom na dugme START otvaraju se dva prozora, svaki za po jednog igrača, i igra kreće. Igra traje dok jedan od igrača ne dođe do ključa. Nakon što brži igrač stigne do ključa, vrši se upis rezultata, a sporijem igraču se prikazuje poruka o gubitku i partija se završava. Igračima se zatim prikazuje glavni meni.

**Akteri:**
Dva igrača

**Preduslovi:**
Aplikacija je pokrenuta, otvoren je glavni meni i uspostavljena je konekcija između dva igrača.

**Postuslovi:**
Jedan igrač je pobedio i igračima je prikazan glavni meni.

**Glavni tok:**
1. Igrač iz glavnog menija bira dugme START.
2. Igračima se omogućava da izaberu opciju multiplayer partije.
3. Igračima se omogućava da unesu svoja imena.
4. Prelazi se na slučaj upotrebe 'Uspostavljanje konekcije sa serverom'.
5. Prelazi se na slučaj upotrebe 'Igra'.
6. Kada se završi partija, uradi sledeće:
   - 6.1. Ukoliko je igrač stigao do cilja, prelazi se na slučaj upotrebe "Čuvanje rezultata".
   - 6.2. Ukoliko je igrača savladala prepreka, pređi na korak 7.
7. Aplikacija prikazuje glavni meni.

**Alternativni tokovi:**
A1: Neočekivan izlaz iz aplikacije - Ukoliko neko od igrača izađe iz aplikacije pre nego što je igra završena, pobeda pripada suprotnom igraču.

**Podtokovi:**
/

**Specijalni zahtevi:**
Neophodno je da imamo tačno dva igrača koji su povezani na isti server.
