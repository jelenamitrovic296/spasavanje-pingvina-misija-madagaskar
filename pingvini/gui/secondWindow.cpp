#include "./secondWindow.h"
#include "ui_secondWindow.h"
#include"./mainwindow.h"

SecondWindow::SecondWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui:: SecondWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("IGRAČ BROJ 2");

    ui ->stackedWidget ->setCurrentIndex(0);
    this->setFixedSize(800, 600);

    //Pozadina
    bSetter = new backgroundSetter(this, ":/resources/nameSurname.jpeg");
    bSetter->setNewBackground();

    //postavljanje stoperice
    ui->lcdNTimer->setFixedSize(100,50);
    ui->lcdNTimer->move(10,10);
    //konekcija
    connect(ui->pbStart2, SIGNAL(clicked()), this, SLOT(startStopwatch()));
    connect(&stopwatch_, SIGNAL(timeChanged(const QTime&)), this, SLOT(updateStopwatchTime(const QTime&)));
}

SecondWindow::~SecondWindow()
{
    delete ui;
}

void SecondWindow::on_pbBack2_clicked()
{
    ui->lMessage2->setText("");
    close();
}

void SecondWindow::on_pbStart2_clicked() {
   //add new window for player 2
    // Otvaranje novog prozora
   //ui ->stackedWidget ->setCurrentIndex(1);
    QString name1 = ui->teTwoPlayers->toPlainText();
    if(name1!=""){
        //game = new Game();
        //game->show();
        //hide();
        ui ->stackedWidget ->setCurrentIndex(1);
        ui->lMessage2->setText("");
    }else{
        ui->lMessage2->setText("You did not enter a name!");
    }
}

void SecondWindow::startStopwatch()
{
    stopwatch_.start();
}

void SecondWindow::updateStopwatchTime(const QTime& time)
{
    ui->lcdNTimer->display(time.toString("hh:mm:ss"));
}
