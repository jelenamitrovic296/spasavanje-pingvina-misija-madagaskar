#ifndef MAPITEM_H
#define MAPITEM_H

#include <QGraphicsPixmapItem>

class mapItem : public QGraphicsPixmapItem{
public:
    mapItem(QString imgPath);

private:
    qint16 scaleFactorX = 50;
    qint16 scaleFactorY = 50;
};

#endif // MAPITEM_H
