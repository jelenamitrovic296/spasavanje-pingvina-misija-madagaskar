# Spasavanje pingvina Misija Madagaskar

Dva igrača(pingvina) se u odvojenim prozorima (imamo dva različita pogleda na scenu) bore sa preprekama i cilj je da stignu do kraja i uzmu ključ kako bi oslobodili Lenu iz kaveza. 

Članovi:
 - <a href="https://gitlab.com/ivana_neskovic">Ivana Nešković 167/2019</a>
 - <a href="https://gitlab.com/velickovicana">Ana Veličković 170/2019</a>
 - <a href="https://gitlab.com/ivanavuckovic">Ivana Vučković 197/2019</a>
 - <a href="https://gitlab.com/jelenamitrovic296">Jelena Mitrović 357/2020</a>
 - <a href="https://gitlab.com/mpapovic27">Marija Papović 63/2019</a>