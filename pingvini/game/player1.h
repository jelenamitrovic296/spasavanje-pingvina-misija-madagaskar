#ifndef PLAYER1_H
#define PLAYER1_H

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsItem>

class Player1 : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Player1();
    void keyPressEvent(QKeyEvent * event);
signals:
    void playerMoved(int newX);
};

#endif // PLAYER1_H
