# Slučaj upotrebe "Uspostavljanje konekcije sa serverom"

## Kratak opis:
Svaki igrač šalje TCP zahtev kako bi se povezao na server. Nakon uspešnog povezivanja moguće je pokrenuti igru klikom na dugme START.

## Akteri:
- Igrači

## Preduslovi:
- Mora biti izabrana opcija igra za dva igrača.

## Postuslovi:
- Igrači su uspešno povezani na server i mogu da započnu igru.

## Osnovni tok:
1. Klijent šalje TCP zahtev serveru kako bi se uspostavila konekcija.
2. Server prihvata zahtev.
3. Uspostavlja se TCP konekcija između servera i klijenta.

## Alternativni tokovi:
- A1: Neuspela konekcija - povratak na glavni meni

## Podtokovi:
- /

## Specijalni zahtevi:
- Klijenti moraju imati pristup internetu.
- Server aplikacija mora biti pokrenuta.

## Dodatne informacije:
- /