#include "./level.h"
#include <QTextStream>


Level::Level()
{

}

Level::Level(QGraphicsScene* scene, QGraphicsView* view)
{
    this->view = view;
    this->scene = scene;

    this->sceneSizeX = Game::sceneSizeX;
    this->sceneSizeY = Game::sceneSizeY;
}

void Level::startLevel(){

   // scene = new QGraphicsScene(0, 0, 800, 600);


//view = new QGraphicsView(scene);

   // QString backgroundImage = ":/resources/sky1.jpg";
   // view->setBackgroundBrush(QPixmap(backgroundImage).scaledToHeight(600));

   // view->resize(1704, 960);

   // view->setFocus();

    this->parseLevelMap();
  //  view->show();
}



void Level::parseLevelMap(){
    QString filename = ":/resources/fajl.txt";
    QFile file(filename);
    if(!file.exists()){
        qDebug() << "File does not exists";
        return;
    }

    if(!file.open(QIODevice::ReadOnly)){
        qDebug() << "Opening failed";
        return;
    }

    QTextStream in(&file);
    QStringList line = in.readLine().split(" ");
    qreal sizeX = line[0].toInt();
    qreal sizeY = line[1].toInt();

    for(int y = 0; y < sizeY; y++){
        QString sceneObjects = in.readLine();
        for (int x = 0; x < sizeX; x++){
            addObject(sceneObjects[x].toLatin1(), x * sceneSizeX / sizeX, (y + 1)  * sceneSizeY / sizeY);
        }
    }

    file.close();
}

void Level::addObject(char symbol, qreal x, qreal y){
    picturesMapLevel *map = new picturesMapLevel();
    if(symbol != '-'){
        mapItem *item = new mapItem(map->getPath(symbol));
        QTextStream(stdout) << x;
        QTextStream(stdout) << ":";
        QTextStream(stdout) << y;
        QTextStream(stdout) << "\n";
        item->setPos(x, y - map->getYDrop(symbol));
        //item->setScale(0);
        scene->addItem(item);
    }
}
