#include "lemur.h"

Lemur::Lemur(QGraphicsItem *parent) : QObject(), QGraphicsPixmapItem(parent)
{
    setPixmap(QPixmap(":/resources/lemur.png").scaled(QSize(50,50)));
    setPos(300, 300);

    direction = 1;
    distance = 50;
    currentDistance = 0;
}

void Lemur::move()
{
    qreal dx = direction * 20;

    currentDistance += qAbs(dx);
    if (currentDistance >= distance)
    {
        currentDistance = 0;
        direction = -direction;
    }

    setPos(x() + dx, y());
}
