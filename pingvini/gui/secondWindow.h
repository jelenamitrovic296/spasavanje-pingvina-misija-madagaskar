#ifndef SECONDWINDOW_H
#define SECONDWINDOW_H
#include "./game/stopwatch.h"
#include <QDialog>
#include "../gui/backgroundSetter.h"

namespace Ui {
class SecondWindow;
}

class SecondWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SecondWindow(QWidget *parent = nullptr);
    ~SecondWindow();

private slots:
    void on_pbBack2_clicked();

    void on_pbStart2_clicked();
    void startStopwatch();
    void updateStopwatchTime(const QTime& time);

private:
    Ui::SecondWindow *ui;
    stopwatch stopwatch_;
    backgroundSetter *bSetter;
};


#endif // SECONDWINDOW_H
